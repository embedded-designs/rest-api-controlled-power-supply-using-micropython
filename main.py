from BK1787 import BK1787
from microdot import Microdot
from NetworkConfig import NetworkConfig
import machine
import uasyncio as asyncio


class BK1787Controller:
    def __init__(self):
        """
        Initializes the class by configuring the network connection, app, and power supply handler.
        Turns on the LED once initialization is complete.

        Parameters:
        ----------
        None

        Returns:
        -------
        None
        """
        # Configure handler to LED
        onboard_led = machine.Pin("LED", machine.Pin.OUT)
        onboard_led.off()

        # Configure the network connection, app, and power supply handler
        self.network_conn = NetworkConfig()
        self.app = Microdot()
        self.ps = BK1787(tx_pin=16, rx_pin=17, baudrate=9600)

        # Define routes
        self.app.get("/")(self.index)
        self.app.get("/controller")(self.controller)
        self.app.get("/measure")(self.measure)
        self.app.post("/set")(self.set_voltage_current)

        # Turn on the LED to indicate initialization has completed.
        onboard_led.on()

    async def index(self, request):
        """
        An asynchronous function that serves as the index route handler.

        Parameters:
            request (Request): The HTTP request object.

        Returns:
            dict: A dictionary containing the status of the request.
                The dictionary has the following structure:
                {
                    "status": str
                }
                - status (str): The status of the request, which is always "OK".
        """
        return {"status": "OK"}

    async def controller(self, request):
        """
        Generates a controller function comment.

        Args:
            request: The HTTP request object.

        Returns:
            html_content: The HTML content to be rendered.
            status_code: The HTTP status code.
            headers: The dictionary of headers.
        """
        html_content = """
        <!DOCTYPE html>
        <html>
            <head><title>BK1787 Controller</title></head>
            <body>
                <h1>BK1787 Power Supply Control</h1>
                <form action="/set" method="post">
                    <label for="voltage">Voltage:</label>
                    <input type="text" id="voltage" name="voltage"><br><br>
                    <label for="current">Current:</label>
                    <input type="text" id="current" name="current"><br><br>
                    <input type="submit" value="Set Values">
                </form>
            </body>
        </html>
        """
        return html_content, 200, {"Content-Type": "text/html"}

    async def measure(self, request):
        """
        Performs a measurement and returns the voltage and current values.

        Args:
            request: The request object.

        Returns:
            A dictionary containing the voltage and current measurements.
        """
        measurements = self.ps.measure()
        return {"voltage": measurements["voltage"], "current": measurements["current"]}

    async def set_voltage_current(self, request):
        """
        Sets the voltage and current values of the power supply based on the provided request.

        Args:
            request (object): The request object containing the voltage and current values.

        Returns:
            dict: A dictionary containing the responses of setting the voltage and current values.

        Raises:
            dict: A dictionary containing the error message if an exception occurs.
        """
        try:
            content_type = request.headers.get("Content-Type")

            # Check if the content type is form-urlencoded
            if content_type == "application/x-www-form-urlencoded":
                data = request.form
            elif content_type == "application/json":
                data = request.json
            else:
                return {"error": f"Unsupported Content-Type: {content_type}"}

            VALID_PARAMETERS = ["voltage", "current"]
            responses = []

            for param in VALID_PARAMETERS:
                if param in data:
                    try:
                        validated_value = float(data[param])
                        if param == "voltage":
                            responses.append(self.ps.set_voltage(validated_value))
                        elif param == "current":
                            responses.append(self.ps.set_current(validated_value))
                    except ValueError:
                        return {"error": f"Invalid {param} value: {data[param]}"}

            return {"responses": responses}
        except Exception as e:
            return {"error": str(e)}

    def run(self):
        """
        Run the application.

        This function starts the application and listens on port 80.

        Parameters:
            None

        Returns:
            None
        """
        self.app.run(port=80)


# Create an instance of the controller and run the app
controller = BK1787Controller()
controller.run()

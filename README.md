# Rest API Controlled Power Supply using MicroPython
A web enabled using MicroPython to control an old power supply via UART

## Requirements
Create a Wi-fi credentials file called `creds.txt` and upload it to the same directory where all `.py` files are uploaded to on your device. `creds.txt` should be in this format:
```
<Network SSID>
<Password>
```
For example:
```
MyLovelyRouter
JustAnotherPassw0rd
```

## Hardware Setup
The Raspberry Pi Pico device contains several UART pins. This code has been tested with `UART0` (`GPIO16` and `GPIO17`). 

### Wiring
To connect the Raspberry Pi Pico device to the BK Precision 1787 power supply simply hook up the following:

| Raspberry Pi Pico Pin       | BK Precision 1787 RS-232 Pin |
| --------------------------- | ---------------------------- |
| `GPIO16` (physical pin 21)  | Pin 3 (Data In)              |
| `GPIO17` (physical pin 22)  | Pin 2 (Data Out)             |
| `GND` (any ground pin)      | Pin 5 (Signal GND)           |

### Pinout References
For reference here are the pinouts on the BK Precision 1787 power supply and the Raspberry Pi Pico:

![BK Precision 1787 RS-232 Connector](BK1787_RS232_Pinout.png)

![Raspberry Pi Pico Pinout](raspberry_pi_Pico-R3-Pinout-narrow.png)

## Webservice
To read out the voltage and current navigate to `http://<ip-address-of-device>/measure`. A JSON object will return the voltage and current that looks like so:
```
{"current": 1.4, "voltage": 3.5}
```

To set the voltage/current you will need to issue a `POST` to `http://<ip-address-of-device>/set`.
An example to set the voltage using Curl:
```
curl -X POST -H "Content-Type: application/json" -d '{"voltage": "3.5"}' http://<ip-address-of-device>/set
```
An example to set the current using Curl:
```
curl -X POST -H "Content-Type: application/json" -d '{"current": "1"}' http://<ip-address-of-device>/set
```

Alternatively, you can use the basic webform by navigating to `http://<ip-address-of-device>/controller`

## Installation

To install the dependencies for this project, follow these steps:

1. Clone the repository: `git clone https://github.com/your-username/your-project.git`
2. Install the required packages: `pip install -r requirements.txt`
3. Set up the Wi-Fi credentials as mentioned in the Requirements section.

## Usage

To use this project, follow these steps:

1. Connect the Raspberry Pi Pico device to the BK Precision 1787 power supply as described in the Hardware Setup section.
2. Upload the `.py` files to your device.
3. Run the main script: `python main.py`

## Troubleshooting

If you encounter any issues while using this project, try the following troubleshooting steps:

1. Make sure the Wi-Fi credentials are correctly set up in the `creds.txt` file.
2. Double-check the wiring connections between the Raspberry Pi Pico device and the BK Precision 1787 power supply.
3. Ensure that the required packages are installed and up to date.

## Contributing

We welcome contributions from the community. If you would like to contribute to this project, please follow these guidelines:

1. Fork the repository and create a new branch.
2. Make your changes and test thoroughly.
3. Submit a pull request describing your changes.

## License

This project is licensed under the GNU Public License - see the [LICENSE](LICENSE) file for details.

## Acknowledgements
Thanks to [microdot](https://github.com/miguelgrinberg/microdot) for providing a FastAPI like webservice out of the box for MicroPython.
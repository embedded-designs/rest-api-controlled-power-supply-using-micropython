import network
import time


class NetworkConfig:
    def __init__(self):
        """
        Initializes the class instance.
        Note: Credentials are read from the 'creds.txt' file.

        Parameters:
            None

        Returns:
            None
        """
        self.ssid, self.password = self.read_creds()
        self.wlan = network.WLAN(network.STA_IF)
        self.connect_to_network()

    def read_creds(self):
        """
        Reads the credentials from the 'creds.txt' file and returns the SSID and password.

        Returns:
            ssid (str): The SSID read from the file.
            password (str): The password read from the file.
        """
        ssid = None
        password = None
        try:
            with open("creds.txt", "r") as file:
                lines = file.readlines()
                if len(lines) >= 2:
                    ssid = lines[0].strip()
                    password = lines[1].strip()
        except OSError as e:
            print("Error: ", str(e))

        # Error checking
        if ssid is None or password is None:
            print("Failed to read credentials from file.")

        return ssid, password

    def connect_to_network(self):
        """
        Connects to a network using the provided SSID and password.

        This function activates the WLAN, disables power-save mode, and connects to the network using the provided SSID and password. It waits for a maximum of 10 seconds for the connection to be established. If the connection fails, it raises a `RuntimeError`. If the connection is successful, it prints the SSID and the IP address.

        Parameters:
            self: The instance of the class.

        Returns:
            None
        """
        self.wlan.active(True)
        self.wlan.config(pm=0xA11140)  # Disable power-save mode
        self.wlan.connect(self.ssid, self.password)

        max_wait = 10
        while max_wait > 0:
            if self.wlan.status() < 0 or self.wlan.status() >= 3:
                break
            max_wait -= 1
            print("waiting for connection...")
            time.sleep(1)

        if self.wlan.status() != 3:
            raise RuntimeError("network connection failed")
        else:
            print("Connected to SSID:", self.ssid)
            status = self.wlan.ifconfig()
            print("ip = " + status[0])

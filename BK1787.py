import machine
import time


## This class contains all the functions needed to control the BK Precision 1787 Power Supply
class BK1787:
    def __init__(self, tx_pin: int, rx_pin: int, baudrate: int = 9600):
        """
        Initializes the UART communication with the device.

        Args:
            tx_pin (int): The pin number for the transmit (TX) signal.
            rx_pin (int): The pin number for the receive (RX) signal.
            baudrate (int, optional): The baudrate for the UART communication. Default is 9600.

        Returns:
            None
        """
        # Configure UART on the device
        self.uart = machine.UART(
            0,
            baudrate=baudrate,
            tx=machine.Pin(tx_pin),
            rx=machine.Pin(rx_pin),
            invert=machine.UART.INV_TX | machine.UART.INV_RX,
        )

    @staticmethod
    def format_number(number, multiplier):
        """
        Format the given number by multiplying it by multiplier, rounding it to the nearest integer,
        and padding it with leading zeros to make it 3 digits long.

        Parameters:
            number (int or float): The number to be formatted.

        Returns:
            str: The formatted number as a string with leading zeros.
        """
        multiplied = number * multiplier
        rounded = round(multiplied)
        str_num = str(rounded)
        padding_length = 3 - len(str_num)
        padded = "0" * padding_length + str_num
        return padded

    def send_command(self, command: str):
        """
        Sends a command to the device and returns the response.

        Args:
            command (str): The command to send to the device.

        Returns:
            str: The response from the device.
        """
        # Fire off the command
        self.uart.write(command + "\r")

        # Grab the response
        time.sleep(0.1)
        return self.uart.readline().decode("utf-8")

    @staticmethod
    def check_set_command(command, response):
        """
        Check if the set command is successful.

        Args:
            command (str): The set command to be checked.
            response (str): The response received from the server.

        Returns:
            dict: A dictionary containing the status of the command and the command itself.
                If the command is successful, the status will be "success" and the command will be returned.
                If the command is not successful, the status will be "error", along with the error message and the command.

        Example Usage:
            >>> check_set_command("SET key value", "OK\r\n")
            {'status': 'success', 'command': 'SET key value'}

            >>> check_set_command("SET key value", "ERROR\r\n")
            {'status': 'error', 'message': 'ERROR', 'command': 'SET key value'}
        """
        sent_command, status = response.split("\r")[0:2]

        if status == "OK" and sent_command == command:
            return {"status": "success", "command": sent_command}
        else:
            return {"status": "error", "message": status, "command": command}

    def set_voltage(self, voltage: float):
        """
        Set the voltage of the device.

        Args:
            voltage (float): The voltage to set, must be between 0 and 60V.

        Returns:
            dict: A dictionary with the status of the command execution and an optional error message.
        """
        # Validate that the input is within the range of 0-60V
        if voltage < 0 or voltage > 60:
            return {"status": "error", "message": "Voltage must be between 0 and 60V"}

        command = f"VOLT{self.format_number(voltage, multiplier=10)}"
        return self.check_set_command(command, self.send_command(command))

    def set_current(self, current: float):
        """
        Sets the current value for the device.

        Args:
            current (float): The desired current value to set. Must be within the range of 0-1.5A.

        Returns:
            dict: A dictionary containing the status and message. If the current is invalid, it returns {"status": "error", "message": "Current must be between 0 and 1.5A"}. Otherwise, it returns the result of the check_set_command function.

        """
        # Validate that the input is within the range of 0-1.5A
        if current < 0 or current > 1.5:
            return {"status": "error", "message": "Current must be between 0 and 1.5A"}

        command = f"CURR{self.format_number(current, multiplier=100)}"
        return self.check_set_command(command, self.send_command(command))

    def measure(self):
        """
        Retrieves measurement data from the device and returns it as a dictionary.

        Parameters:
            None

        Returns:
            dict: A dictionary containing the measurement data. The structure of the dictionary is as follows:
                - 'status' (str): The status of the measurement, either 'success' or 'error'.
                - 'voltage' (float): The measured voltage in volts.
                - 'current' (float): The measured current in amperes.
                - 'command' (str): The command used to retrieve the measurement data.
                - 'raw_data' (str): The raw data received from the device.

            If the status is 'success', the measurement was successful and the 'voltage', 'current', 'command', and 'raw_data'
            fields will contain valid values. If the status is 'error', an error occurred during the measurement and the 'message'
            field will contain a description of the error.

        """
        response = self.send_command("GETD")
        command, data, status = response.split("\r")[0:3]

        if status == "OK":
            voltage = int(data[:3]) / 10
            current = int(data[3:]) / 100
            return {
                "status": "success",
                "voltage": voltage,
                "current": current,
                "command": command,
                "raw_data": data,
            }
        else:
            return {"status": "error", "message": status, "command": command}


if __name__ == "__main__":
    # Basic test to validate measurements
    ps = BK1787(tx_pin=16, rx_pin=17, baudrate=9600)

    for i in range(10):
        print(ps.set_voltage(i))
        print(ps.set_current(i / 10))
        time.sleep(0.5)
        print(ps.measure())
        time.sleep(1)
